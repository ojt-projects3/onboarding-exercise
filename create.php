<?php
    include_once 'dbcon.php';

    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $age = $_POST['age'];

        $sql = "INSERT INTO users(name, email, age)
                                  VALUES('$name','$email','$age')";

        if($conn->query($sql) === TRUE){
            header("Location: index.php");
        } 
        else{
            echo "Error:" . $sql . "<br>" . $conn->error;
        }
    }
    
    $conn->close();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
</head>
<body>
    <h2>Create User</h2>
    <form action="" method="POST">
        <label for="name">Name: <input type="text" name="name"> </label>
        <label for="email">Email: <input type="email" name="email"></label>
        <label for="age"> Age: <input type="Number" name="age"></label>
        <input type="submit" value="Create">
    </form>
</body>
</html>
