<?php
include_once('dbcon.php');

$sql = "SELECT * FROM users";
$results = $conn->query($sql);
$users = [];
if($results->num_rows > 0){
    while($row = $results->fetch_assoc()){
        $users[] = $row;
    }
     
}

$conn->close();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index | Read</title>
</head>
<body>
    <h2>User List</h2>
    <table>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Age</th>
        </tr>
        <?php foreach($users as $user){ ?>
                <tr>
                <td><?php echo $user['name']; ?></td>
                <td><?php echo $user['email']; ?></td>
                <td><?php echo $user['age']; ?></td>
                <td><a href="edit.php?id=<?= $user['id']?>">Edit</a> <a href="delete.php?id=<?= $user['id']?>">Delete</a></td>
                </tr>

        <?php } ?>
    </table>
    
    <a href="create.php">Create New User</a>
</body>
</html>
