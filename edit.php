<?php
include_once('dbcon.php');
?>
<?php

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $id = $_POST['id'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $age = $_POST['age'];

    $sql = "UPDATE users SET name = '$name', email = '$email', age = '$age' WHERE id=$id";
    if($conn->query($sql) === TRUE){
        header("Location: index.php");
    }
}
elseif (isset($_GET['id'])){
    $id = $_GET['id'];
    $sql = "SELECT * FROM users WHERE id='$id'";
    $result = $conn->query($sql);
    if($result->num_rows == 1){
        $user = $result->fetch_assoc();
    }
    else{
        echo "User not Found";
        exit;
    }
}

$conn->close();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
</head>
<body>
    <h2>Update User</h2>
    <form action="" method="POST">
        <input type="hidden" name="id" value="<?= $user['id'] ?>">
        <label for="name">Name: <input type="text" name="name" value="<?= $user['name'] ?>"> </label>
        <label for="email">Email: <input type="email" name="email" value="<?= $user['email'] ?>"></label>
        <label for="age"> Age: <input type="Number" name="age" value="<?= $user['age'] ?>"></label>
        <input type="submit" value="Update">
    </form>

        <a href="index.php">Back to list</a>
</body>
</html>
